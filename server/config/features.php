<?php

/*
 * Feature flags for application behaviour.
 */

$config['features'] = [
    'authenticate' => false,
    'content_negotiation' => false,
];

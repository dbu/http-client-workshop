<?php

namespace Todo\Serializer;

use Todo\Todo;

interface Serializer
{
    /**
     * Serialize the data into an encoding.
     *
     * @param array $data
     *
     * @return string
     */
    public function serialize(array $data);

    /**
     * Unserialize a Todo that was serialized by this serializer.
     *
     * @param string $data
     *
     * @return Todo
     */
    public function unserialize($data);
}

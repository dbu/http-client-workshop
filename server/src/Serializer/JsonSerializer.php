<?php

namespace Todo\Serializer;

class JsonSerializer implements Serializer
{
    public function serialize(array $data)
    {
        return json_encode($data);
    }

    public function unserialize($data)
    {
        return json_decode($data, true);
    }
}

<?php

namespace Todo;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * A simplistic repository that stores and loads Todo objects in the filesystem.
 */
class TodoRepository
{
    private $storagePath;

    /**
     * @param string $storagePath Absolute path to the storage for Todo items.
     */
    public function __construct($storagePath)
    {
        $this->storagePath = $storagePath;
    }

    /**
     * @param string $id Load a todo by ID.
     *
     * @return Todo
     *
     * @throws NotFoundException
     */
    public function loadTodo($id)
    {
        $path = $this->getPath($id);
        if (!file_exists($path)) {
            throw new NotFoundException("No todo with id $id");
        }

        return unserialize(file_get_contents($path));
    }

    /**
     * Creates a new entry if id is not set, otherwise updates that entry.
     *
     * @param Todo $todo The Todo to save.
     */
    public function storeTodo(Todo $todo)
    {
        if (!$todo->id) {
            $todo->id = md5($todo->title.uniqid());
        }

        file_put_contents($this->getPath($todo->id), serialize($todo));
    }

    /**
     * Deletes an item. Does not check if the item existed or not.
     *
     * @param string $id Id of item to delete.
     */
    public function removeTodo($id)
    {
        unlink($this->getPath($id));
    }

    /**
     * @return Todo[] A subset of the Todos in this repository.
     */
    public function findTodos($offset, $limit)
    {
        $files = array_slice($this->listAllTodoFiles(), $offset, $limit);

        return array_map(function (SplFileInfo $file) {
            return unserialize($file->getContents());
        }, $files);
    }

    /**
     * @return int The total number of Todos in this repository.
     */
    public function countTodos()
    {
        return count($this->listAllTodoFiles());
    }

    /**
     * Get the absolute path to the file for a Todo item.
     *
     * @param string $id Todo item id.
     *
     * @return string Absolute path to file for that item.
     */
    private function getPath($id)
    {
        return $this->storagePath.DIRECTORY_SEPARATOR.$id.'.bin';
    }

    /**
     * @return \Iterator|\Symfony\Component\Finder\SplFileInfo[]
     */
    private function listAllTodoFiles()
    {
        return array_values(iterator_to_array(
            (new Finder())
                ->in($this->storagePath)
                ->name('*.bin')
                ->sortByName()
                ->getIterator()
        ));
    }
}

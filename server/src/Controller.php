<?php

namespace Todo;
use Symfony\Component\OptionsResolver\Exception\InvalidArgumentException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todo\Serializer\Serializer;

/**
 * Handle HTTP operations on a single item.
 */
class Controller
{
    /**
     * @var TodoRepository
     */
    private $todoRepository;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var string
     */
    private $id;

    public function __construct(TodoRepository $todoRepository, Serializer $serializer, $id)
    {
        $this->todoRepository = $todoRepository;
        $this->serializer = $serializer;
        $this->id = $id;
    }

    public function get()
    {
        header('Cache-Control: max-age=600');
        header('Content-Type: application/json');

        return $this->serializer->serialize($this->loadTodo()->toArray());
    }

    public function put()
    {
        $existing = $this->loadTodo();

        $resolver = new OptionsResolver();
        $resolver->setRequired(['id', 'completed', 'title']);
        $resolver->setAllowedTypes('id', 'string');
        $resolver->setAllowedTypes('completed', 'bool');
        $resolver->setAllowedTypes('title', 'string');

        try {
            $data = $resolver->resolve($this->serializer->unserialize(file_get_contents('php://input')));
        } catch (InvalidArgumentException $e) {
            http_response_code(400);
            echo json_encode([
                'code' => 404,
                'message' => $e->getMessage(),
            ]);
            return;
        }
        $todo = new Todo($data);
        if ($todo->id !== $existing->id) {
            http_response_code(400);
            echo json_encode([
                'code' => 400,
                'message' => 'ID in content and in query string do not match.',
            ]);
            return;
        }

        $this->todoRepository->storeTodo($todo);
        http_response_code(204);
    }

    public function delete()
    {
        $this->todoRepository->removeTodo($this->id);
        http_response_code(204);
    }

    /**
     * Get the item and abort request if item does not exist.
     *
     * @return Todo
     */
    private function loadTodo()
    {
        try {
            return $this->todoRepository->loadTodo($this->id);
        } catch (NotFoundException $e) {
            http_response_code(404);
            echo json_encode([
                'code' => 404,
                'message' => 'No TODO with id '.$_GET['id']
            ]);
            exit;
        }
    }
}

<?php

namespace Todo;

/**
 * A Todo item was not found.
 */
class NotFoundException extends \Exception
{}

<?php

namespace Todo;

/**
 * Model class representing a TODO entry.
 */
class Todo
{
    /**
     * Set fields from data.
     *
     * @param array $data completed and title are mandatory, id is optional.
     */
    public function __construct(array $data)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        $this->completed = (bool) $data['completed'];
        $this->title = $data['title'];
    }

    /**
     * @var string If not set, this is a new TODO.
     */
    public $id;

    /**
     * @var bool Whether this item is done.
     */
    public $completed;

    /**
     * @var string Body of this item.
     */
    public $title;

    public function toArray()
    {
        return [
            'id' => $this->id,
            'completed' => $this->completed,
            'title' => $this->title,
        ];
    }
}

<?php

/*
 * POST: Create a new TODO item.
 *
 * Body is expected to be JSON encoded:
 *
 * {,
 *     "completed": false,
 *     "title": "Test TODO",
 * }
 *
 * Returns the newly created TODO item.
 *
 * Example:
 *
 * curl -X POST -d '{"completed": false, "title": "my new TODO"}' http://localhost:8000/create.php
 */

use Symfony\Component\OptionsResolver\Exception\InvalidArgumentException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Todo\Todo;

require('../bootstrap.php');

if ('POST' !== $_SERVER['REQUEST_METHOD']) {
    http_response_code(400);
    echo 'Only POST is allowed.';
    return;
}

$resolver = new OptionsResolver();
$resolver->setRequired(['completed', 'title']);
$resolver->setAllowedTypes('completed', 'bool');
$resolver->setAllowedTypes('title', 'string');

try {
    $data = $resolver->resolve($serializer->unserialize(file_get_contents('php://input')));
} catch (InvalidArgumentException $e) {
    http_response_code(400);
    echo $e->getMessage();
    return;
}

$todo = new Todo($data);
$todoRepository->storeTodo($todo);

http_response_code(201);

echo $serializer->serialize($todo->toArray());

<html>
<head>
    <title>TODO API</title>
    <link href='swaggerui/css/typography.css' media='screen' rel='stylesheet' type='text/css'/>
    <link href='swaggerui/css/reset.css' media='screen' rel='stylesheet' type='text/css'/>
    <link href='swaggerui/css/screen.css' media='screen' rel='stylesheet' type='text/css'/>
    <link href='swaggerui/css/reset.css' media='print' rel='stylesheet' type='text/css'/>
    <link href='swaggerui/css/print.css' media='print' rel='stylesheet' type='text/css'/>

    <script src='swaggerui/lib/jquery-1.8.0.min.js' type='text/javascript'></script>
    <script src='swaggerui/lib/jquery.slideto.min.js' type='text/javascript'></script>
    <script src='swaggerui/lib/jquery.wiggle.min.js' type='text/javascript'></script>
    <script src='swaggerui/lib/jquery.ba-bbq.min.js' type='text/javascript'></script>
    <script src='swaggerui/lib/handlebars-2.0.0.js' type='text/javascript'></script>
    <script src='swaggerui/lib/underscore-min.js' type='text/javascript'></script>
    <script src='swaggerui/lib/backbone-min.js' type='text/javascript'></script>
    <script src='swaggerui/swagger-ui.js' type='text/javascript'></script>
    <script src='swaggerui/lib/highlight.7.3.pack.js' type='text/javascript'></script>
    <script src='swaggerui/lib/jsoneditor.min.js' type='text/javascript'></script>
    <script src='swaggerui/lib/marked.js' type='text/javascript'></script>
    <script src='swaggerui/lib/swagger-oauth.js' type='text/javascript'></script>
    <script type="application/javascript">
        var swaggerUi = new SwaggerUi({
            url: '/swagger.yaml',
            dom_id: 'swagger-ui-container',
            validatorUrl: null,
            docExpansion: 'list'
        });

        swaggerUi.load();
    </script>
</head>

<body class="swagger-section">
<div id='header'>
    <div class="swagger-ui-wrap">
        <a id="logo" href="http://swagger.io">swagger</a>
    </div>
</div>

<div id="message-bar" class="swagger-ui-wrap" data-sw-translate>&nbsp;</div>
<div id="swagger-ui-container" class="swagger-ui-wrap"></div>

</body>

</html>

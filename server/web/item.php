<?php

/*
 * Handle a single TODO item.
 *
 * The item in JSON representation is:
 *
 * {
 *     "id": "abcd",
 *     "completed": false,
 *     "title": "Test TODO",
 * }
 *
 * Depending on the HTTP method, this reads (GET), updates (PUT) or deletes (DELETE) the item.
 * For creating, see create.php.
 *
 * Examples:
 *
 * curl http://localhost:8000/item.php?id=abcd
 * curl -v -X PUT -d '{"id": "abcd", "completed": false, "title": "my new TODO"}' http://localhost:8000/item.php?id=abcd
 * curl -v -X DELETE http://localhost:8000/item.php?id=abcd
 */

use Todo\Controller;

require('../bootstrap.php');

if (!array_key_exists('id', $_GET)) {
    http_response_code(400);
    echo json_encode([
        'code' => 404,
        'message' => 'You need to specify the TODO id in the query string',
    ]);
    return;
}

$controller = new Controller($todoRepository, $serializer, $_GET['id']);

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        echo $controller->get();
        return;

    case 'PUT':
        $controller->put();
        return;

    case 'DELETE':
        $controller->delete();
        return;

    default:
        http_response_code(400);
        echo json_encode([
            'code' => 404,
            'message' => 'This end point only handles GET, PUT and DELETE'
        ]);
        return;
}

<?php

/*
 * GET: List all TODO items.
 *
 * {
 *     "count": 42,
 *     "data": [
 *         {
 *             "id": "abcd",
 *             "completed": false,
 *             "title": "Test TODO",
 *         },
 *         ...
 *     ]
 * }
 *
 * Parameters:
 *  - offset: First item to return. Defaults to 0.
 *  - limit:  Maximum number of items to return. Defaults to 10.
 *
 * Example:
 *
 * curl http://localhost:8000/list.php
 */

use Todo\Todo;

require('../bootstrap.php');

$offset = array_key_exists('offset', $_GET) ? $_GET['offset'] : 0;
$limit = array_key_exists('limit', $_GET) ? $_GET['limit'] : 10;

header('Cache-Control: max-age=600');

echo $serializer->serialize([
    'count' => $todoRepository->countTodos(),
    'data' => array_map(function (Todo $todo) {
        return $todo->toArray();
    }, $todoRepository->findTodos($offset, $limit)),
]);

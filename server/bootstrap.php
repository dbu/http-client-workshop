<?php

use Todo\TodoRepository;
use Todo\Serializer\JsonSerializer;

require('vendor/autoload.php');
require('config/config.php');

if ($config['features']['authenticate']) {
    throw new \Exception('Implement authentication');
}

if ($config['features']['content_negotiation']) {
    throw new \Exception('Implement content negotiation');
} else {
    $serializer = new JsonSerializer();
}

header('Content-Type: application/json');
$todoRepository = new TodoRepository($config['storage_directory']);

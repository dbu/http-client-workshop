#!/bin/bash

cd server && composer install
cd ../clients
cp symfony-app/app/config/parameters.yml.dist symfony-app/app/config/parameters.yml
cp symfony-skeleton/app/config/parameters.yml.dist symfony-skeleton/app/config/parameters.yml
for D in *; do
    if [ -d "${D}" ]; then
        cd "${D}" && composer install && cd ..
    fi
done

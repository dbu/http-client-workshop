Http Client Tutorial
====================

This tutorial is about writing HTTP clients in PHP. To give the clients something to do, the
tutorial consists of a server application as well.

The application is inspired by http://todomvc.com/ but is split into a server API and various
clients.

Setup
-----

*When using the web summer camp virtualbox, setup has already been run and everything is ready.*

```bash
./installation/run.sh
```

Server
------

The server side is a simplistic API to manage TODO items. The data format is:

```
{
    "id": "abcd",
    "completed": false,
    "title": "my new TODO"
}
```

See [the swagger doc](http://api.httplug.websc/) for information on the calls you can do.

Usage
-----

There are several evolutions of client applications. Look in the clients folder:

### Trivial

The trivial client consists of the PHP script `bin/client` which creates a TODO item and then lists all existing TODO items.

### Library

The library is an example how to build a reusable library to use an API. 
The actual library is in the src/ folder.

The repository also contains a simplistic application to use the library in the form of PHP scripts in the bin/ folder.
Bootstrapping the simplistic application is done in the app/ folder.

### Symfony-App

The Symfony application integrates the TODO library into Symfony with a bundle and does a web interface on top of the API.
This Symfony app has no database of its own but only talks to the TODO API.

It uses the HttplugBundle to configure the client and to show requests in the toolbar.

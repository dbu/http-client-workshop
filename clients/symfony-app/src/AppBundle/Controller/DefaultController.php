<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Wsc\Component\Todo\Exception\NotFoundException;
use Wsc\Component\Todo\Model\Todo;
use Wsc\Component\Todo\TodoClient;
use Wsc\TodoBundle\Form\TodoType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig', [
            'todos' => $this->getClient()->getTodoPage(),
            'form' => $this->createForm(TodoType::class)->createView(),
        ]);
    }

    /**
     * @Route("/", name="todo_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $todo = new Todo('todo');
        $form = $this->createForm(TodoType::class, $todo);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getClient()->storeTodo($todo);

            return new RedirectResponse($this->generateUrl('todo_detail', ['id' => $todo->getId()]));
        }

        return $this->render('default/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/todo/{id}", name="todo_detail")
     * @Method("GET")
     *
     * @param string $id The todo item id
     */
    public function detailAction($id)
    {
        return $this->render('default/detail.html.twig', [
            'todo' => $this->getTodo($id),
        ]);
    }

    /**
     * Set a todo item to completed.
     *
     * @Route("/todo/{id}/complete", name="todo_complete")
     * @Method("PUT")
     *
     * @param string $id The todo item id
     */
    public function completeAction(Request $request, $id)
    {
        $todo = $this->getTodo($id);

        $completed = 'false' !== $request->getContent();
        $todo->setCompleted($completed);
        $this->getClient()->storeTodo($todo);

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Remove a todo item.
     *
     * @Route("/todo/{id}", name="todo_delete")
     * @Method("DELETE")
     *
     * @param string $id The todo item id
     *
     * @return Response The body is the target URL. Status is 200, because ajax follows redirections.
     */
    public function deleteAction($id)
    {
        $this->getClient()->deleteTodo($id);

        return new Response($this->generateUrl('homepage'));
    }

    /**
     * @param string $id
     *
     * @return Todo
     *
     * @throws NotFoundHttpException If no item with $id is found
     */
    private function getTodo($id)
    {
        try {
           return $this->getClient()->getTodo($id);
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException('No TODO item with id '.$id);
        }
    }

    /**
     * @return TodoClient
     */
    private function getClient()
    {
        return $this->container->get('wsc_todo.client');
    }
}

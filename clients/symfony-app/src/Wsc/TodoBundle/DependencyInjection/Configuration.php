<?php

namespace Wsc\TodoBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('wsc_todo');

        $rootNode
            ->children()
                ->scalarNode('client')
                    ->info('Service name of the HTTP client')
                    ->isRequired()
                ->end()
                ->scalarNode('message_factory')
                    ->info('Service name of the message factory')
                    ->defaultValue('httplug.message_factory')
                ->end()
                ->scalarNode('stream_factory')
                    ->info('Service name of the stream factory')
                    ->defaultValue('httplug.stream_factory')
                ->end()
            ->end();

        return $treeBuilder;
    }
}

Symfony TODO Frontend
=====================

This is a simple web application without its own storage that uses the todo API for data.

WscTodoBundle
-------------

The TODO bundle brings the TODO library into Symfony. It offers configuration, a form type and validation.

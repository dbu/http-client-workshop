<?php

namespace Wsc\Component\Todo\Model;

/**
 * Collection of TODO items.
 *
 * Depending on the amount of items on the server and what limit and offset is used, this could be
 * a subset of what is on the server.
 *
 * The key in the iterator is the item id.
 */
class TodoCollection implements \IteratorAggregate
{
    /**
     * @var Todo[]
     */
    private $items;

    /**
     * @var int
     */
    private $totalCount;

    /**
     * @param array $items      List of TODO items of a batch
     * @param int   $totalCount Total count of items
     */
    public function __construct(array $items, $totalCount)
    {
        $this->items = $items;
        $this->totalCount = $totalCount;
    }

    /**
     * Get total count of TODO items.
     *
     * Depending on the offset and limit parameters, this collection may contain less items than
     * the total count.
     *
     * @return int
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }

    /**
     * Fullfill the IteratorAggregate contract.
     *
     * @return \Traversable
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }
}

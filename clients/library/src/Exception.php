<?php

namespace Wsc\Component\Todo;

/**
 * Every TODO Client related Exception implements this interface.
 */
interface Exception
{
}

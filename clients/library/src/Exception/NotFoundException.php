<?php

namespace Wsc\Component\Todo\Exception;

use Wsc\Component\Todo\Exception;

/**
 * Thrown when a todo item is not found.
 */
class NotFoundException extends \RuntimeException implements Exception
{
}

<?php

namespace Wsc\Component\Todo\Exception;

use Wsc\Component\Todo\Exception;

/**
 * Thrown when the API returns an error.
 */
class UnkownErrorException extends \RuntimeException implements Exception
{
}

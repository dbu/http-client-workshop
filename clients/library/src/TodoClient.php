<?php

namespace Wsc\Component\Todo;

use Http\Client\Exception as HttplugException;
use Http\Client\HttpClient;
use Http\Discovery\MessageFactoryDiscovery;
use Http\Discovery\StreamFactoryDiscovery;
use Http\Message\RequestFactory;
use Http\Message\StreamFactory;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Wsc\Component\Todo\Exception\InvalidArgumentException;
use Wsc\Component\Todo\Exception\NotFoundException;
use Wsc\Component\Todo\Exception\UnkownErrorException;
use Wsc\Component\Todo\Model\Todo;
use Wsc\Component\Todo\Model\TodoCollection;

/**
 * API client library for the TODO API.
 */
class TodoClient
{
    const LIST_ITEMS = '/list.php';
    const ITEM = '/item.php';
    const CREATE_ITEM = '/create.php';

    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var RequestFactory
     */
    private $requestFactory;

    /**
     * @var StreamFactory
     */
    private $streamFactory;

    /**
     * The id is immutable on the model.
     *
     * When a new TODO item is created, the API tells us the ID it assigned to the new item.
     * We need the item to have that id and rather update the existing item than return a new copy.
     *
     * @var \ReflectionProperty
     */
    private $idReflection;

    /**
     * TodoClient constructor.
     *
     * @param HttpClient $httpClient A properly configured HttpClient
     * @param RequestFactory|null $requestFactory
     * @param StreamFactory|null $streamFactory
     *
     * @see HttpClientFactory::createClient
     */
    public function __construct(
        HttpClient $httpClient,
        RequestFactory $requestFactory = null,
        StreamFactory $streamFactory = null
    ) {
        $this->httpClient = $httpClient;
        $this->requestFactory = $requestFactory ?: MessageFactoryDiscovery::find();
        $this->streamFactory = $streamFactory ?: StreamFactoryDiscovery::find();

        // @see self::$idReflection
        $reflectionClass = new \ReflectionClass(Todo::class);
        $this->idReflection = $reflectionClass->getProperty('id');
        $this->idReflection->setAccessible(true);
    }

    /**
     * Get a single page of the Todo list.
     *
     * If you go beyond the last item, an empty list is returned.
     *
     * @param int $offset First item to get
     * @param int $limit  Number of items to get
     *
     * @return TodoCollection A collection with the items.
     */
    public function getTodoPage($offset = 0, $limit = 10)
    {
        $parameters = [];
        if ($offset) {
            $parameters['offset'] = (int) $offset;
        }
        if (10 !== $limit) {
            $parameters['limit'] = (int) $limit;
        }
        $uri = static::LIST_ITEMS;
        if (count($parameters)) {
            $uri .= '?'.http_build_query($parameters);
        }
        $request = $this->requestFactory->createRequest('GET', $uri);

        $response = $this->sendRequest($request);

        $list = json_decode($response->getBody(), true);
        $items = [];
        foreach ($list['data'] as $data) {
            $item = Todo::fromArray($data);
            $items[$item->getId()] = $item;
        }

        return new TodoCollection($items, $list['count']);
    }

    /**
     * Get an item identified by the id.
     *
     * @param string $id
     *
     * @return Todo
     *
     * @throws NotFoundException If the server does not find an item with this ID.
     */
    public function getTodo($id)
    {
        if (!$id) {
            throw new InvalidArgumentException('Id may not be empty');
        }
        $uri = static::ITEM.'?'.http_build_query(['id' => $id]);
        $request = $this->requestFactory->createRequest('GET', $uri);
        $response = $this->sendRequest($request);

        $data = json_decode($response->getBody(), true);

        return Todo::fromArray($data);
    }

    /**
     * Convenience method to create or update an item.
     *
     * @param Todo $item
     */
    public function storeTodo(Todo $item)
    {
        $item->isNew() ? $this->createTodo($item) : $this->updateTodo($item);
    }

    /**
     * Create a new item.
     *
     * @param Todo $item The todo item to create on the API.
     */
    public function createTodo(Todo $item)
    {
        if (!$item->isNew()) {
            throw new InvalidArgumentException(sprintf('Item %s already exists', $item->getId()));
        }
        $uri = static::CREATE_ITEM;
        $body = $this->streamFactory->createStream(json_encode($item->toArray()));
        $request = $this->requestFactory
            ->createRequest('POST', $uri)
            ->withBody($body)
        ;
        $response = $this->sendRequest($request);

        $data = json_decode($response->getBody(), true);

        $this->idReflection->setValue($item, $data['id']);
    }

    /**
     * Update an existing item.
     *
     * @param Todo $item
     *
     * @return Todo the item that has been updated.
     *
     * @throws NotFoundException If the server does not find an item with this ID.
     */
    public function updateTodo(Todo $item)
    {
        $uri = static::ITEM.'?'.http_build_query(['id' => $item->getId()]);
        $body = $this->streamFactory->createStream(json_encode($item->toArray()));
        $request = $this->requestFactory
            ->createRequest('PUT', $uri)
            ->withBody($body)
        ;
        $this->sendRequest($request);

        return $item;
    }

    /**
     * Delete an item by id.
     *
     * @param string $id
     *
     * @throws NotFoundException If the server does not find an item with this ID.
     */
    public function deleteTodo($id)
    {
        $uri = static::ITEM.'?'.http_build_query(['id' => $id]);
        $request = $this->requestFactory->createRequest('DELETE', $uri);
        $this->sendRequest($request);
    }

    /**
     * Send a request and handle request errors.
     *
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     *
     * @throws NotFoundException    When the TODO item is not found
     * @throws UnkownErrorException When the request failed for a different reason
     */
    private function sendRequest(RequestInterface $request)
    {
        try {
            $response = $this->httpClient->sendRequest($request);

            if ($response->getStatusCode() > 299 || $response->getStatusCode() < 200) {
                switch ($response->getStatusCode())
                {
                    case 404:
                        throw new NotFoundException('TODO item not found', 0);
                        break;

                    default:
                        throw new UnkownErrorException('Unknown error occurred during processing the request', 0);
                        break;
                }
            }

            return $response;
        } catch (HttplugException $e) {
            throw new UnkownErrorException('Unknown error occurred during processing the request', 0, $e);
        }
    }
}

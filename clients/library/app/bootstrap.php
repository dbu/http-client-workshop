<?php

use Http\Client\Common\Plugin\AuthenticationPlugin;
use Http\Message\Authentication\BasicAuth;
use Wsc\Component\Todo\TodoClient;
use Wsc\Component\Todo\HttpClientFactory;

require(__DIR__.'/../vendor/autoload.php');

function setup()
{
    require(__DIR__.'/config.php');

    $plugins = [];
    if (isset($username, $password)) {
        $plugins[] = new AuthenticationPlugin(
            new BasicAuth($username, $password)
        );
    }

    $httpClient = HttpClientFactory::createClient($server, $plugins);

    return new TodoClient($httpClient);
}

$client = setup();

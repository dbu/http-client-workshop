<?php

namespace Tests\Wsc\Component\Todo;

use Http\Client\Exception;
use Http\Client\Exception\TransferException;
use Http\Discovery\MessageFactoryDiscovery;
use Http\Discovery\StreamFactoryDiscovery;
use Http\Message\MessageFactory;
use Http\Mock\Client as MockClient;
use Psr\Http\Message\ResponseInterface;
use Wsc\Component\Todo\Exception\InvalidArgumentException;
use Wsc\Component\Todo\Exception\NotFoundException;
use Wsc\Component\Todo\Exception\UnkownErrorException;
use Wsc\Component\Todo\Model\Todo;
use Wsc\Component\Todo\Model\TodoCollection;
use Wsc\Component\Todo\TodoClient;

class TodoClientTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var MessageFactory
     */
    private static $messageFactory;

    /**
     * @var MockClient
     */
    private $httpClient;

    /**
     * @var TodoClient
     */
    private $todoClient;

    public static function setUpBeforeClass()
    {
        static::$messageFactory = MessageFactoryDiscovery::find();
    }

    public function setUp()
    {
        $this->httpClient = new MockClient();
        $this->todoClient = new TodoClient($this->httpClient);
    }

    /**
     * @dataProvider getTodoPageProvider
     */
    public function testGetTodoPage(array $data)
    {
        $data = [
            'count' => $count = count($data),
            'data' => $data,
        ];

        $response = static::$messageFactory->createResponse(
            200,
            null,
            [],
            json_encode($data)
        );

        $this->httpClient->addResponse($response);

        $todos = $this->todoClient->getTodoPage();

        static::assertInstanceOf(TodoCollection::class, $todos);
        static::assertCount($count, $todos);

        $request = $this->httpClient->getRequests()[0];

        self::assertEquals('GET', $request->getMethod());
        self::assertEquals('/list.php', (string) $request->getUri());
    }

    public function getTodoPageProvider()
    {
        return [
            [
                [
                    [
                        'id' => 1,
                        'completed' => true,
                        'title' => 'Todo 1',
                    ],
                    [
                        'id' => 2,
                        'completed' => false,
                        'title' => 'Todo 2',
                    ],
                ],
            ],
            [[]],
        ];
    }

    public function testGetTodo()
    {
        $response = static::$messageFactory->createResponse(
            200,
            null,
            [],
            json_encode([
                'id' => 1,
                'completed' => true,
                'title' => 'Todo 1',
            ])
        );

        $this->httpClient->addResponse($response);

        $todo = $this->todoClient->getTodo(1);

        static::assertInstanceOf(Todo::class, $todo);

        $request = $this->httpClient->getRequests()[0];

        self::assertEquals('GET', $request->getMethod());
        self::assertEquals('/item.php?id=1', (string) $request->getUri());
    }

    public function testGetTodoInvalidId()
    {
        $this->expectException(InvalidArgumentException::class);

        $this->todoClient->getTodo(null);
    }

    /**
     * @dataProvider getTodoErrorProvider
     */
    public function testGetTodoErrors($exception, $responseOrException)
    {
        $this->setUpHttpError($exception, $responseOrException);

        $this->todoClient->getTodo(1);
    }

    public function getTodoErrorProvider()
    {
        $messageFactory = MessageFactoryDiscovery::find();

        return [
            [
                NotFoundException::class,
                $messageFactory->createResponse(404),
            ],
            [
                UnkownErrorException::class,
                $messageFactory->createResponse(500),
            ],
            [
                UnkownErrorException::class,
                new TransferException(),
            ],
        ];
    }

    public function testCreateTodo()
    {
        $response = static::$messageFactory->createResponse(
            201,
            null,
            [],
            json_encode([
                'id' => 1,
                'completed' => false,
                'title' => 'Todo 1',
            ])
        );

        $this->httpClient->addResponse($response);

        $todo = new Todo('Todo 1');

        $this->todoClient->createTodo($todo);

        static::assertEquals(1, $todo->getId());

        $request = $this->httpClient->getRequests()[0];

        self::assertEquals('POST', $request->getMethod());
        self::assertEquals('/create.php', (string) $request->getUri());
    }

    public function testCreateTodoNotNew()
    {
        $this->expectException(InvalidArgumentException::class);

        $todo = Todo::fromArray([
            'id' => 1,
            'completed' => false,
            'title' => 'Todo 1',
        ]);

        $this->todoClient->createTodo($todo);
    }

    /**
     * @dataProvider createTodoErrorProvider
     */
    public function testCreateTodoErrors($exception, $responseOrException)
    {
        $this->setUpHttpError($exception, $responseOrException);

        $this->todoClient->createTodo(new Todo('Todo 1'));
    }

    public function createTodoErrorProvider()
    {
        $messageFactory = MessageFactoryDiscovery::find();

        return [
            [
                UnkownErrorException::class,
                $messageFactory->createResponse(500),
            ],
            [
                UnkownErrorException::class,
                new TransferException(),
            ],
        ];
    }

    public function testUpdateTodo()
    {
        $response = static::$messageFactory->createResponse(
            204,
            null,
            [],
            json_encode([
                'id' => 1,
                'completed' => false,
                'title' => 'Todo 1',
            ])
        );

        $this->httpClient->addResponse($response);

        $todo = Todo::fromArray([
            'id' => 1,
            'completed' => false,
            'title' => 'Todo 1',
        ]);

        $updatedTodo = $this->todoClient->updateTodo($todo);

        static::assertSame($todo, $updatedTodo);

        $request = $this->httpClient->getRequests()[0];

        self::assertEquals('PUT', $request->getMethod());
        self::assertEquals('/item.php?id=1', (string) $request->getUri());
    }

    /**
     * @dataProvider updateTodoErrorProvider
     */
    public function testUpdateTodoErrors($exception, $responseOrException)
    {
        $this->setUpHttpError($exception, $responseOrException);

        $todo = Todo::fromArray([
            'id' => 1,
            'completed' => false,
            'title' => 'Todo 1',
        ]);

        $this->todoClient->updateTodo($todo);
    }

    public function updateTodoErrorProvider()
    {
        $messageFactory = MessageFactoryDiscovery::find();

        return [
            [
                NotFoundException::class,
                $messageFactory->createResponse(404),
            ],
            [
                UnkownErrorException::class,
                $messageFactory->createResponse(500),
            ],
            [
                UnkownErrorException::class,
                new TransferException(),
            ],
        ];
    }

    public function testDeleteTodo()
    {
        $this->todoClient->deleteTodo(1);

        $request = $this->httpClient->getRequests()[0];

        self::assertEquals('DELETE', $request->getMethod());
        self::assertEquals('/item.php?id=1', (string) $request->getUri());
    }

    /**
     * @dataProvider deleteTodoErrorProvider
     */
    public function testDeleteTodoErrors($exception, $responseOrException)
    {
        $this->setUpHttpError($exception, $responseOrException);

        $this->todoClient->deleteTodo(1);
    }

    public function deleteTodoErrorProvider()
    {
        $messageFactory = MessageFactoryDiscovery::find();

        return [
            [
                NotFoundException::class,
                $messageFactory->createResponse(404),
            ],
            [
                UnkownErrorException::class,
                $messageFactory->createResponse(500),
            ],
            [
                UnkownErrorException::class,
                new TransferException(),
            ],
        ];
    }

    /**
     * @param string                      $exception
     * @param ResponseInterface|Exception $responseOrException
     */
    private function setUpHttpError($exception, $responseOrException)
    {
        $this->expectException($exception);

        if ($responseOrException instanceof ResponseInterface) {
            $this->httpClient->addResponse($responseOrException);
        } elseif ($responseOrException instanceof Exception) {
            $this->httpClient->addException($responseOrException);
        } else {
            throw new \InvalidArgumentException('You must pass a Response or an HTTP Exception');
        }
    }
}

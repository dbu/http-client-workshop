<?php

namespace Tests\Wsc\Component\Todo;

use Http\Client\Exception;
use Http\Client\Exception\TransferException;
use Http\Discovery\MessageFactoryDiscovery;
use Http\Discovery\StreamFactoryDiscovery;
use Http\Message\MessageFactory;
use Http\Mock\Client as MockClient;
use Psr\Http\Message\ResponseInterface;
use Wsc\Component\Todo\Exception\InvalidArgumentException;
use Wsc\Component\Todo\Exception\NotFoundException;
use Wsc\Component\Todo\Exception\UnkownErrorException;
use Wsc\Component\Todo\Model\Todo;
use Wsc\Component\Todo\Model\TodoCollection;
use Wsc\Component\Todo\TodoClient;

class TodoClientTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var MessageFactory
     */
    private static $messageFactory;

    /**
     * @var MockClient
     */
    private $httpClient;

    /**
     * @var TodoClient
     */
    private $todoClient;

    public static function setUpBeforeClass()
    {
        static::$messageFactory = MessageFactoryDiscovery::find();
    }

    public function setUp()
    {
        $this->httpClient = new MockClient();
        $this->todoClient = new TodoClient($this->httpClient);
    }

    public function testGetTodoPage()
    {
    }

    public function testGetTodo()
    {
    }

    public function testGetTodoInvalidId()
    {
    }

    public function testGetTodoErrors()
    {
    }

    public function testCreateTodo()
    {
    }

    public function testCreateTodoNotNew()
    {
    }

    public function testCreateTodoErrors()
    {
    }

    public function testUpdateTodo()
    {
    }

    public function testUpdateTodoErrors()
    {
    }

    public function updateTodoErrorProvider()
    {
    }

    public function testDeleteTodo()
    {
    }

    public function testDeleteTodoErrors()
    {
    }
}

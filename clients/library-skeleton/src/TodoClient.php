<?php

namespace Wsc\Component\Todo;

use Http\Client\Exception as HttplugException;
use Http\Client\HttpClient;
use Http\Discovery\MessageFactoryDiscovery;
use Http\Discovery\StreamFactoryDiscovery;
use Http\Message\RequestFactory;
use Http\Message\StreamFactory;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Wsc\Component\Todo\Exception\InvalidArgumentException;
use Wsc\Component\Todo\Exception\NotFoundException;
use Wsc\Component\Todo\Exception\UnkownErrorException;
use Wsc\Component\Todo\Model\Todo;
use Wsc\Component\Todo\Model\TodoCollection;

/**
 * API client library for the TODO API.
 */
class TodoClient
{
    const LIST_ITEMS = '/list.php';
    const ITEM = '/item.php';
    const CREATE_ITEM = '/create.php';

    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var RequestFactory
     */
    private $requestFactory;

    /**
     * @var StreamFactory
     */
    private $streamFactory;

    public function __construct(
        // TODO
    ) {
        throw new \Exception('todo');
    }

    /**
     * Get a single page of the Todo list.
     *
     * If you go beyond the last item, an empty list is returned.
     *
     * @param int $offset First item to get
     * @param int $limit  Number of items to get
     *
     * @return TodoCollection A collection with the items.
     */
    public function getTodoPage($offset = 0, $limit = 10)
    {
        throw new \Exception('todo');
    }

    /**
     * Get an item identified by the id.
     *
     * @param string $id
     *
     * @return Todo
     *
     * @throws NotFoundException If the server does not find an item with this ID.
     */
    public function getTodo($id)
    {
        throw new \Exception('todo');
    }

    /**
     * Convenience method to create or update an item.
     *
     * @param Todo $item
     */
    public function storeTodo(Todo $item)
    {
        throw new \Exception('todo');
    }

    /**
     * Create a new item.
     *
     * @param Todo $item The todo item to create on the API.
     */
    public function createTodo(Todo $item)
    {
        throw new \Exception('todo');
    }

    /**
     * Update an existing item.
     *
     * @param Todo $item
     *
     * @return Todo the item that has been updated.
     *
     * @throws NotFoundException If the server does not find an item with this ID.
     */
    public function updateTodo(Todo $item)
    {
        throw new \Exception('todo');
    }

    /**
     * Delete an item by id.
     *
     * @param string $id
     *
     * @throws NotFoundException If the server does not find an item with this ID.
     */
    public function deleteTodo($id)
    {
        throw new \Exception('todo');
    }
}

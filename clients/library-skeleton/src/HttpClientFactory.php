<?php

namespace Wsc\Component\Todo;

use Http\Client\Common\Plugin\AddHostPlugin;
use Http\Client\Common\Plugin\AuthenticationPlugin;
use Http\Client\Common\PluginClient;
use Http\Client\HttpClient;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\UriFactoryDiscovery;
use Http\Message\Authentication;
use Http\Message\MessageFactory;
use Http\Message\StreamFactory;
use Psr\Http\Message\UriInterface;

class HttpClientFactory
{
    /**
     * Wrap client with the necessary behaviour to work with the API.
     *
     * @param UriInterface|string $host       URL of the API host
     * @param array               $plugins    Additional plugins for the HTTP client
     * @param HttpClient|null     $httpClient Basic client instance if you don't want to use discovery
     *
     * @return PluginClient
     */
    public static function createClient($host, array $plugins = [], HttpClient $httpClient = null)
    {
        if (is_string($host)) {
            $host = UriFactoryDiscovery::find()->createUri($host);
        }
        if (!$host instanceof UriInterface) {
            throw new \InvalidArgumentException('server uri must be string or a PSR-7 UriInterface');
        }
        if (!$host->getHost()) {
            throw new \InvalidArgumentException('server uri must specify the host: "'.$host.'""');
        }

        $plugins[] = new AddHostPlugin($host);

        if (!$httpClient) {
            $httpClient = HttpClientDiscovery::find();
        }

        return new PluginClient($httpClient, $plugins);
    }
}

<?php

namespace Wsc\Component\Todo\Exception;

use Wsc\Component\Todo\Exception;

/**
 * Thrown when invalid arguments are passed to the TodoClient.
 */
class InvalidArgumentException extends \RuntimeException implements Exception
{
}

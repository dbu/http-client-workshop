<?php

namespace Wsc\Component\Todo\Model;

/**
 * Model class representing a TODO entry.
 *
 * This class also contains the methods toArray and fromArray to handle serialization.
 * This is *not* a best practice. A better approach would be to use a dedicated serializer component
 * like symfony serializer or jms serializer to translate between XML / json and objects.
 */
class Todo
{
    /**
     * @var string If not set, this is a new TODO.
     */
    private $id;

    /**
     * @var bool Whether this item is done.
     */
    private $completed;

    /**
     * @var string Body of this item.
     */
    private $title;

    /**
     * Create a new TODO item.
     *
     * @param string $title     The title for this item
     * @param bool   $completed Whether this item is already done
     */
    public function __construct($title, $completed = false)
    {
        $this->title = $title;
        $this->completed = (bool) $completed;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool Whether this is a new item or an existing one.
     */
    public function isNew()
    {
        return null === $this->id;
    }

    /**
     * @return boolean
     */
    public function isCompleted()
    {
        return $this->completed;
    }

    /**
     * @param boolean $completed
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Array representation of the item.
     *
     * @return array
     *
     * @internal
     */
    public function toArray()
    {
        $data = [
            'completed' => $this->completed,
            'title' => $this->title,
        ];
        if ($this->id) {
            // only include id if it exists, for new items
            $data['id'] = $this->id;
        }

        return $data;
    }

    /**
     * Restore the TODO item from an API response.
     *
     * @param array $data id, completed and title are all mandatory.
     *
     * @return Todo
     *
     * @internal
     */
    public static function fromArray(array $data)
    {
        $todo = new Todo($data['title'], (bool) $data['completed']);
        $todo->id = $data['id'];

        return $todo;
    }
}

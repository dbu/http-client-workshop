<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Wsc\Component\Todo\Exception\NotFoundException;
use Wsc\Component\Todo\Model\Todo;
use Wsc\Component\Todo\TodoClient;
use Wsc\TodoBundle\Form\TodoType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method("GET")
     */
    public function indexAction()
    {
        throw new ServiceUnavailableHttpException('Not yet implemented');
    }

    /**
     * @Route("/", name="todo_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        throw new ServiceUnavailableHttpException('Not yet implemented');
    }

    /**
     * @Route("/todo/{id}", name="todo_detail")
     * @Method("GET")
     *
     * @param string $id The todo item id
     */
    public function detailAction($id)
    {
        throw new ServiceUnavailableHttpException('Not yet implemented');
    }

    /**
     * Set a todo item to completed.
     *
     * @Route("/todo/{id}/complete", name="todo_complete")
     * @Method("PUT")
     *
     * @param string $id The todo item id
     */
    public function completeAction(Request $request, $id)
    {
        throw new ServiceUnavailableHttpException('Not yet implemented');
    }

    /**
     * Remove a todo item.
     *
     * @Route("/todo/{id}", name="todo_delete")
     * @Method("DELETE")
     *
     * @param string $id The todo item id
     *
     * @return Response The body is the target URL. Status is 200, because ajax follows redirections.
     */
    public function deleteAction($id)
    {
        throw new ServiceUnavailableHttpException('Not yet implemented');
    }

    /**
     * @return TodoClient
     */
    private function getClient()
    {
        return $this->container->get('wsc_todo.client');
    }
}
